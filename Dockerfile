FROM python:3

# Copy library, DVC and model
COPY setup.py /app/setup.py
COPY README.md /app/README.md
COPY model/ /app/model
COPY outputs/model.p /app/outputs/model.p

WORKDIR /app
ENV ENV prod
RUN pip install .

CMD run_prepare --prod; run_features --prod; run_predict --prod;

