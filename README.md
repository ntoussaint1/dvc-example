# DVC Example
This repository had been made to show you how DVC can improve the workflow in a ML project. It is a small demonstration for how to work with DVC with real data. It will show you how to store your models, how to share them and how to get back to one specific.

In this example, you will work on a metro traffic dataset provided by UCI. You will learn :
   
* How to build an experiment
* How to share your experiment
* How to go back and visualize experiments.

Also, we have work on a makefile to help you get through the basic of DVC. SO this tutorial will work like that, first you will see the basic command to work with DVC, then you will see how we do it with a make file command. 


## Installation
This example do not require any heavy modules. Just the basics. To install them you can use the setup:

    pip install . 
    
Then you need import data, to do that a make rule had been created. It will download the UCI dataset and put it in a data folder.

    make get-data
    
You will want to retrieve the experiments made by the team. To do that, the .dvc/config file should contain a URL to an external cloud (S3, GCP, Azure etc.). And you would be able to retrieve data from it. In this example we mocked this. We set a local folder as a remote in the config file. To mock, we run all the experiment locally with a make file. THIS IS NOT THE CORRECT WAY TO DO IT, usually you will have your own remote ready however for the tutorial we wanted you to have already some experiment ready :

    make create-remote

Step by step, it goes back to each tag of Git, run the experiment and push it to the local remote.

## Let's build your first experiment

You have already the pipeline built in the folder pipeline. The source files are in the folder model. 

You can start by changing the model in train.py. You can choose any you want. Then, you can check what steps have to be run to run the experiment.

    dvc status

This command will show you the dependancies that have change and what should be run.
Then you can run the pipeline :

    dvc repro pipeline/evaluate.dvc

You can also use the shortcut of the Dvcfile

    dvc repro

That's it you have your pipeline runned and now up to date.

You can now look at its performance with the metrics saved during the run :

    dvc metrics show

The metric is defined in the evaluate.py.

# Let's share your experiment

You are happy, your model is working pretty well, you want to save it and share it to the team.
To do so, you have first to be sure that the experiment is up to date !

    dvc status

If nothing needs to be run, you can save the new dvc files, to see which :

    git status

Then you have to add all the .dvc and .py you have changed to save the experiment in Git :

    git add # ......
    git commit # .....
    git tag # ... a name to go back to it easily

That's it Git has saved your source and the REFERENCE to your outputs. Thus you now need to save the outputs with dvc.

    dvc push

That's quick, no ? Your expriment is saved and you and your colleague can go back to it at anytime.

# Let's go back to an experiment

As describe in the article, to go back to some experiement means go back to the source and the dvc files referencing the data.
To do so, you have first to go back to the commit. The easiest way is to have tags on the experiments.

First let's see the model available, to do so you can ask dvc to show you all metrics for all tags :

    dvc pull -T pipeline/evaluate.dvc # First you need to pull the metrics data (Dvc will only pull this and not other data)
    dvc metrics show -T # Dvc show you all the tags with theirs metrics

Now you can choose the best one or another and go back to it :

    git checkout #commit id or tag name
    dvc pull # pull the data from the remote
    

That's it you have your experiment as it was, you can check it with :

    dvc status 

It should tell you that all is up to date. You can now make your updates on a new branch and save it again.

# Shortcut using a makefile

You have seen that you have to do quit a lot of commands to save an expriment and also you have to do it in the right order.
To simplify this, we have built a simple make command that will help you save your expriment. It simply does all the commands :
 
    make experiment

Then you check that all the right files have been added to Git and give a name to your experiment.

You can also see quickly the expriments :

    make visualize

Also you can go back to an expriment with the good data with :

    make goback-#tag name

If you want to see all the commands you can do :

    make help
    
