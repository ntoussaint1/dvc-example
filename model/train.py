# Imports
import logging

import pandas as pd
from sklearn import linear_model

from config import OUTPUT_TRAIN, OUTPUT_SPLIT


def train(X, y):
    model = linear_model.Ridge()
    model.fit(X, y)
    return model


if __name__ == "__main__":
    input_dict = pd.read_pickle(OUTPUT_SPLIT)

    print(input_dict)
    X, y = input_dict["X_train"], input_dict["y_train"]
    model = train(X, y)

    pd.to_pickle(model, OUTPUT_TRAIN)

    logging.info("A model has been trained with %s features named :\n\t%s.", len(X.columns), ", ".join(X.columns))
